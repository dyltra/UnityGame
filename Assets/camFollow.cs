﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camFollow : MonoBehaviour
{
    public GameObject target;
    private Vector3 relPosition;
    // Start is called before the first frame update
    void Start()
    {
        relPosition = transform.position - target.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = target.transform.position + relPosition;
    }
}
