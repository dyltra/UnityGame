﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zombieBehavior : MonoBehaviour
{
    public int defaultHealth = 100;
    private int health;
    public int defaultStrength = 20;
    private int strength = 20;
    private int state;

    // Start is called before the first frame update
    void Start()
    {
        health = defaultHealth;
        strength = defaultStrength;
    }

    // Update is called once per frame
    void Update()
    {
        switch(state){
            case 1:
                idle();
            break;
            case 2:
                chase();
            break;
            case 3:
                attack();
            break;
            default:
                death();
            break;
        }
    }

    //idle state
    private void idle(){
        //idles and wanders
    }

    //prey detection
    private void chase(){
        //turns on navigation to player
    }

    //attack
    private void attack(){
        //removes health from player
    }

    public void damage(int health){
        //take damage from player attack
        this.health -= health;
        if (health<=0){
            state = 4;
        }
    }

    //death
    private void death(){
        //plays death animation
        //destroys current instance of Zombie object
    }
}
